#!/bin/bash

# Constants
USER_USERNAME="mheath@deloitte.co.uk.so1"
USER_EMAIL="mheath@deloitte.co.uk"
USER_ALIAS="admin-user"
USER_PERMSETS="FinancialServicesCloudBasic,FinancialServicesCloudStandard,FinancialServicesCloudExtension"
USER_PERMSETS_STR="FinancialServicesCloudBasic FinancialServicesCloudStandard FinancialServicesCloudExtension"
SO_ALIAS="fscscratch2"
SO_DEF="config/project-scratch-def.json"
#FSC_PACKAGE="FinancialServicesCloud@232.1"
#FSC_EXTENSION="FinancialServicesCloud@218.1"
#Packages:
#http://industries.force.com/financialservicescloud
#http://industries.force.com/financialservicescloudextension

FSC_PACKAGE="04t1E000000jbFt"
FSC_EXTENSION="04t1E000001Iql5"
DIR_MANAGED="base-managed"
DIR_UNLOCKED="base-unlocked"

setup() {
        createOrg
        createUser
        installPackage $FSC_PACKAGE
        installPackage $FSC_EXTENSION
        push $DIR_MANAGED
        push $DIR_UNLOCKED
}

createOrg()
{
        printf "${HEADLINE}Creating Scratch Org${END}\n"
        sfdx force:org:create -f $SO_DEF -a $SO_ALIAS -s
}
createUser() {
        printf "${HEADLINE}Creating User${END}\n"
        sfdx force:user:create --setalias $USER_ALIAS permsets=$USER_PERMSETS defaultusername=$USER_USERNAME adminEmail=$USER_EMAIL generatepassword=true
}
userAssignPerms() {
        for ps in $USER_PERMSETS_STR; do 
                sfdx force:user:permset:assign -u $SO_ALIAS -n $ps
        done
}
createUserCustom() {
        printf "${HEADLINE}Creating Custom User from user-def.json${END}\n"
        sfdx force:user:create --setalias $USER_ALIAS --definitionfile config/user-def.json generatepassword=true
}

# Install package
installPackage() {
        printf "${HEADLINE}Installing $1 ${END}\n"
        sfdx force:package:install -u $SO_ALIAS --package $1 -w 30
}
installPackageA() {
        printf "${HEADLINE}Installing $1 ${END}\n"
        sfdx force:package:install -u $SO_ALIAS --package $FSC_PACKAGE -w 30
        
}
# To push packages
push() {
        printf "${HEADLINE}Deploying from dir $1 ${END}\n"
        sfdx force:source:deploy -p $1
        #Push will only push default directory
        #sfdx force:source:push -u $SO_ALIAS    
}
openOrg() {
        printf "${HEADLINE}Opening Scratch Org ${END}\n"
        sfdx force:org:open -u $SO_ALIAS
}


#Styling
HEADLINE='\033[90m=== \033[34m' # Blue
INFO='\033[90m' # Grey
WARNING='\033[1;33m' # Yellow
END='\033[0m' # No Color