#!/bin/bash
source $PL_UTILS

printf "${YELLOW} Creating Scratch Org: $SO_ALIAS ${END}\n"
SO_USER
# --targetdevhubusername used for later when cycling, using alias for now as should be consistent regardless of username
SCRATCH_ORG=$(sfdx force:org:create --definitionfile $SO_DEF --setalias $SO_ALIAS --targetdevhubusername $DH_ALIAS --setdefaultusername orgName=$SO_NAME edition=$SO_EDITION adminEmail=$USER_EMAIL username=$USER_USERNAME  --json)

if [[ $SCRATCH_ORG == *"\"status\": 0"* ]]; then {
    printf "${GREEN} Scratch Org Created Successfully${END}\n"
    DEF_ORG=$(sfdx config:set defaultusername=$SO_ALIAS -g --json)
    if [[ $DEF_ORG == *"\"status\": 0"* ]]; then {
        printf "${GREEN} Scratch Org Set as Default Org${END}\n"
    }; else
        {
            printf "${RED} WARNING: Scratch Org not set as default${END}\n"
            printf "${CYAN} MESSAGE: $DEF_ORG ${END}\n"
        }
    fi
}; else
    {
        printf "${RED} Scratch Org Created Not created${END}\n"
        printf "${CYAN} MESSAGE: $SCRATCH_ORG ${END}\n"
        exit 1
    }
fi
printf "${YELLOW}Assigning permission sets to $USER_USERNAME in $SO_ALIAS ${END}\n"
PS_FAILS=()
for ps in ${USER_PERMSETS[@]}; do
    PS_ASS=$(sfdx force:user:permset:assign -u $SO_ALIAS -n $ps --json)
    if [[ $PS_ASS == *"\"status\": 0"* ]]; then {
        printf "${GREEN} Permission Set assigned: $ps ${END}\n"
    }; else
        {
            PS_FAILS+=($ps)
            printf "${RED} Failed to assign: $ps ${END}\n"
            printf "${CYAN} MESSAGE: $PS_ASS {END}\n"
        }
    fi
done

if [[ ${#PS_FAILS[@]} != 0 ]]; then
    printf "${RED} Failed to assign ${#PS_FAILS[@]}/${#PS_USER_PERMSETSFAILS[@]} permission sets: ${PS_FAILS[@]} ${END}\n"

NEW_USER=$(sfdx force:user:password:generate --json)
if [[ $NEW_USER == *"\"status\": 0"* ]]; then {
    printf "${GREEN} User Details: $NEW_PASS ${END}\n"
}; else
    {
        printf "${RED} Failed to generate new password ${END}\n"
        printf "${CYAN} MESSAGE: $NEW_USER ${END}\n"
    }
fi
