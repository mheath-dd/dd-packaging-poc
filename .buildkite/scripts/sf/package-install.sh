#!/bin/bash
source $PL_UTILS

PKGS=("$@")
PACKAGE_FAILS=()
setVal "package_install_success" "true"
for PKG in ${PKGS[@]}; do
    PACKAGE=$(
        sfdx force:package:install -u $SO_ALIAS \
        --package $PKG \
        --wait 30 \
        --json
    )
    if [[ $PACKAGE == *"\"status\": 0"* ]]; then {
        printf "${GREEN} Package installed: $PKG ${END}\n"
    }; else
        {
            PACKAGE_FAILS+=($PKG)
            printf "${RED} Failed to install: $PKG ${END}\n"
            printf "${CYAN} MESSAGE: $PACKAGE {END}\n"
        }
    fi
done

if [[ ${#PACKAGE_FAILS[@]} != 0 ]]; then
    printf "${RED} Failed to install ${#PACKAGE_FAILS[@]}/${#PKGS[@]} packages: ${PACKAGE_FAILS[@]} ${END}\n"
    setVal "package_install_success" "false"
fi
