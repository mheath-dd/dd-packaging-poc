#!/bin/bash
source $PL_UTILS

PACKAGE_VERSION_ID=$1

if [ -z "$PACKAGE_VERSION_ID" ]; then {
    printf "${RED} Incorrect package version: $PACKAGE_VERSION_ID ${END}\n"
    exit 1
} else {
    printf "${YELLOW} Promoting package: $1 ${END}\n"
}
fi



PACKAGE_PROMOTE=$(sfdx force:package:version:promote \
--package $PACKAGE_VERSION_ID \
--noprompt \
--json)
if [[ $PACKAGE_PROMOTE == *"\"status\": 0"* ]]; then {
    printf "${GREEN} Promoted package version: $PACKAGE_VERSION_ID ${END}\n"
}; else
    {
        printf "${RED} Failed to promote package version: $PACKAGE_VERSION_ID ${END}\n"
        printf "${CYAN} MESSAGE: $PACKAGE_PROMOTE ${END}\n"
    }
fi