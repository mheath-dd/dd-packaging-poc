source .buildkite/scripts/env-vars-local.sh

SO_UN="test-txfcgmlcgbfw@example.com"
AUTH=$(sfdx auth:jwt:grant \
    --username $SO_UN \
    --jwtkeyfile $KEY_DIR \
    --clientid $CONSUMER_KEY \
    --setalias $SO_ALIAS \
    --setdefaultusername \
    --instanceurl https://test.salesforce.com \
    --json)

printf "${GREEN} $AUTH ${END}\n"

